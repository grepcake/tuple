#include <iostream>
#include <memory>

#include <gtest/gtest.h>

#include "tuple.hpp"

template <typename... Args>
using container = tuple<Args...>;

TEST(correctness, empty) {
    std::tuple<> t;
}

TEST(correctness, int) {
    constexpr container<int> t1{5};
    static_assert(get<0>(t1) == 5);
    static_assert(get<int>(t1) == 5);
}

TEST(correcntess, int_and_string) {
    container<int, std::string> t1{5, "hey"};
    EXPECT_EQ(get<0>(t1), 5);
    EXPECT_EQ(get<std::string>(t1), "hey");
}

TEST(correctness, lvalue_ctor) {
    std::string const s = "i'ma const";
    container<int, std::string> t1{7, s};
    EXPECT_EQ(get<1>(t1), s);
}

TEST(correcntess, rvalue_ctor) {
    int a;
    tuple<int, std::unique_ptr<int>> b(a, std::make_unique<int>(42));
}

TEST(correctness, is_constructible) {
    bool t1 = std::is_constructible_v<container<int, std::string>, short, char const *>;
    EXPECT_TRUE(t1);
    bool t2 = std::is_constructible_v<container<long, double>, container<long, double>>;
    EXPECT_TRUE(t2);
    bool t3 = std::is_constructible_v<container<int>, std::string>;
    EXPECT_FALSE(t3);
}

TEST(correcntess, copy_ctor) {
    container<int, long> t1{5, 10};
    container<int, long> t2 = t1;
    EXPECT_EQ(get<0>(t2), 5);
    EXPECT_EQ(get<1>(t2), 10);
}

TEST(correcntess, copy_ctor_direct_initialization) {
    container<int, long> t1{5, 10};
    container<int, long> t2(t1);
    EXPECT_EQ(get<0>(t2), 5);
    EXPECT_EQ(get<1>(t2), 10);
}

TEST(correcntess, ce) {
    container<int, std::string, int> t1{5, "hey", 6};
    EXPECT_EQ(get<0>(t1), 5);
    EXPECT_EQ(get<2>(t1), 6);
    EXPECT_EQ(get<std::string>(t1), "hey");
    //    get<int>(t1); compilation failure
}

TEST(correctness, assign_to_element) {
    container<int, std::string, int> t1{5, "hey", 6};
    get<0>(t1) = 3;
    EXPECT_EQ(get<0>(t1), 3);

    const container<int, long> t2{7, 10};
    //    get<0>(t2) = 5; compilation failure
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
